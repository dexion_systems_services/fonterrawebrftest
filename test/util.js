const {Builder, By, Key, until, WebElement} = require('selenium-webdriver');
var assert = require('assert');
const { HOST_IP } = require('./test_data.js');
const { send } = require('process');
const axios = require('axios')
const fs = require('fs')
const {query} = require('./db.js');
const { time } = require('console');

const DEFAULT_TIMEOUT = 60000
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function expectElementContainsText(driver, elementId, text){
    var connectButtonElement = await driver.findElement(By.id(elementId));
    //console.log('expectElementContainsText()::elementId=', connectButtonElement);
    
    await driver.wait(await until.elementTextContains(connectButtonElement, text), DEFAULT_TIMEOUT)
   // console.log('expectElementContainsText()::elementId text value=',  await connectButtonElement.getText());
}
module.exports.expectElementContainsText = expectElementContainsText

async function userInput(driver, text){
    var inputElement = await driver.findElement(By.id('scan'));
   // console.log('userInput()::element=',await inputElement.getAttribute('id'), ',text=', text);
    assert.strictEqual(await inputElement.getAttribute('id'), 'scan');
    await inputElement.sendKeys(text)
    await inputElement.sendKeys(Key.ENTER)
}
module.exports.userInput = userInput

async function userInputSpecialKey(driver, key){
    await sleep(1000)
    var inputElement = await driver.findElement(By.id('scan'));
    //console.log('userInputSpecialKey key', key, ', scan element =', inputElement)
    await inputElement.sendKeys(Key.ESCAPE)
}
module.exports.userInputSpecialKey = userInputSpecialKey



module.exports.login = async function(driver){
    //login
    // await driver.get('http://192.168.10.116/gui/webrf');
    await driver.get(`http://${HOST_IP}/gui/webrf`);

    var connectButtonElement = await driver.findElement(By.id('connect_btn'));
    //console.log('connectButtonElement=', connectButtonElement);

    await expectElementContainsText(driver, 'connect_btn', 'Press To Connect')
    await connectButtonElement.click()

    await expectElementContainsText(driver, 'heading', 'User Login')
    await userInput(driver, "1")
 

    await expectElementContainsText(driver, 'content1', 'Enter Password')
    await userInput(driver, "1")

}

module.exports.clickF1 = async function(driver){
    await driver.manage().setTimeouts( { implicit: 1000 } );
    var f1ButtonElement = await driver.findElement(By.id('f1_btn'))
    //console.log('clickF1:', f1ButtonElement)
    await f1ButtonElement.click()
}


module.exports.selectMenu = async function(driver, menuText){
    await driver.wait(async function(){
        var inputElements = await driver.findElements(By.className('menu_li'));
        for(var i = 0; i < inputElements.length; i++){
            var text = await inputElements[i].getText();
           // console.log('text=', text)
            if (text == menuText){
                await inputElements[i].click()
                return true
            }
        }
        return false
    }, DEFAULT_TIMEOUT)
}

async function sendHIFCMessage( testFile ){
    // const data = fs.readFileSync('./inbound/inbound1/asn1.xml', 'utf8');
    try{
        const data = fs.readFileSync(testFile, 'utf8');
        axios.post(
            `http://${HOST_IP}/rdsservice/service.php`,
            data,
            {headers:
                {'Content-Type': 'text/xml'}
            }
        ).then((response) => {
            console.log('------------------BEGIN RESPONSE ---------------');
            console.log(response.data);
            console.log('------------------END  RESPONSE ---------------');
        }, (error) => {
            console.log('----------------my ERROR', error);
        });
    }
    catch(e){
        console.error('sendHIFCMessage error=', e)
        throw Error(`Unable to read file=${testFile}`)
    } 
}
module.exports.sendHIFCMessage = sendHIFCMessage

async function expectAsnName(driver, asnName){
    await driver.wait(async function(){
        console.log('expectAsnName()::', asnName)
        var results = await query(`select * from po_shipment ps where shipment_name = '${asnName}'`)
        //console.log('expectAsnName() results=', results.rowCount)
        if (results.rowCount > 0 )
            return true
        return false
    } , DEFAULT_TIMEOUT)
}
module.exports.expectAsnName = expectAsnName

async function expectOrderNumber(driver, orderName){
    await driver.wait(async function(){
        console.log('expectOrderNumber()::', orderName)
        var results = await query(`select * from orders o where o.order_number = '${orderName}'`)
        //console.log('expectAsnName() results=', results.rowCount)
        if (results.rowCount > 0 )
            return true
        return false
    } , DEFAULT_TIMEOUT)
}
module.exports.expectOrderNumber = expectOrderNumber

async function releaseWave(waveName){
    await query(`update wave_header set status_id=(select status_id from statii where status='Released' and context='Wave') where name='${waveName}'`)
    //await query(`update orders set status_id =(select status_id from statii where status='Released' and context='Order') where order_id = 538`)

}
module.exports.releaseWave=releaseWave

async function expectSSCCInAreaList(driver, sscc, areaList, timeout){
    await driver.wait(async function(){
        for (var i = 0; i < areaList.length; i++ ){
            var areaName = areaList[i]
            var results = await query(`select * from container c join inventory inv on c.container_id = inv.container_id 
                        join locations l on l.location_id = inv.location_id 
                        join zones z on z.zone_id = l.zone_id 
                        join areas a on a.area_id = z.area_id 
                        where a.area_name = '${areaName}' and c.container_name='${sscc}'`)
            
            if (results.rowCount > 0 )
                return true
        }
        
        return false
    }, timeout)
}
module.exports.expectSSCCInAreaList = expectSSCCInAreaList

async function userEscape(driver){
    var inputElement = await driver.findElement(By.id('scan'));
    
    assert.strictEqual(await inputElement.getAttribute('id'), 'scan');
    await inputElement.sendKeys(Key.ESCAPE)
}
module.exports.userEscape = userEscape


async function expectInventory(driver, sscc, item_code,  areaList, timeout){
    await driver.wait(async function(){
        /*for (let i = 0; i < areaList.length; i++ ){
          var areaName = areaList[i]*/
        for(let areaName of areaList){
            var results = await query(`select * from container c join inventory inv on c.container_id = inv.container_id 
                        join locations l on l.location_id = inv.location_id 
                        join zones z on z.zone_id = l.zone_id 
                        join areas a on a.area_id = z.area_id 
                        join items i on i.item_id = inv.item_id
                        where a.area_name = '${areaName}' and c.container_name='${sscc}'
                        and i.item_code = '${item_code}'`)
            
            if (results.rowCount > 0 )
                return true
        }
        
        return false
    }, timeout)
}
module.exports.expectInventory = expectInventory


exports.expectSQLResultCount = async function(driver, sql, resultCount){
    var results = await query(sql)
    return results.rowCount;
}