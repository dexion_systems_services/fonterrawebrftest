var  {DB_USERNAME, HOST_IP, HOST_DB} = require('./test_data.js');
const { Client } = require('pg');

async function query(sql){
    //console.log('username=',DB_USERNAME,'IP=', HOST_IP);

    const client = new Client({
        user: DB_USERNAME,
        host: HOST_IP,
        database: HOST_DB,
        password: '',
        port: 5432,
    });

    await client.connect();

    const res =  await client.query(sql)

    await client.end();

    return res

}

async function clearAll(){
    console.log('Db::clearAll() begin')
    await query('delete from inventory')
    await query('delete from receiving')
    await query('delete from item_groups where group_id>0')
    await query('delete from items where item_id>0')
    await query('delete from container where container_id>0')
    await query('delete from tasks where task_id>0')
    await query('delete from putaway ')
    await query('delete from wave_header where wave_id>0')
    await query('delete from picks where pick_id>0')
    await query('delete from orders where order_id>0')
    await query('delete from po_shipment')
    await query('delete from gui_messages')
    await query('delete from  genhosterr_dtl')
    await query('delete from hosterr_hdr')
    await query('delete from inventory_log ')
    await query('update slots set head=0 where head!= 0')
    await query('delete from workload w')
    await query('delete from picking_prod')
    await query('delete from daily_picking_prod')
    await query('delete from operator_prod')
    await query('delete from crane_mission_groups ')
    await query('delete from crane_missions')
    await query('delete from crane_error_log')
    await query('delete from crane_event_log ')
    await query('delete from fonterra_divert_log ')
    await query('delete from fonterra_pallet_infeed_queue')
    await query('DELETE FROM client_connections')
    await query('DELETE FROM webrf_recv_q')
    await query('DELETE FROM connection_profile')
    await query('DELETE FROM session_variables')
    await query('delete from sim_convpos')
    await query('delete from slot_attrib where attribute = \'ContType\'');
    await query('delete from slot_attrib where "attribute" = \'LOCKED\'');
    await query('delete FROM sku_change_log');
    console.log('Db::clearAll() end')
}
module.exports.query = query;
module.exports.clearAll = clearAll;
