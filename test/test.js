const {clearAll} = require('./db.js')
const inbound = require('./inbound/inbound_tests.js')
const picking = require('./picking/picking_test.js')
const skuchange = require('./skuchange/sku_change.js')

// How to run 
// npm run test -- --grep "test name name"
//Eg. npm run test -- --grep "CleanDb1"


describe('CleanDb', function(){
    beforeEach(async function(){
        console.log('Before Each');
        clearAll();
        
    })
    describe('CleanDb', function() {
        it('CleanDb1', async function() { 
            
        });
        
    });
})

describe('Inbound', function() {
   
    beforeEach(async function(){
        console.log('Before Each');
        clearAll();
        
    })

    describe('inbound1. 4 SSCC into N10 of RECV03 via DEIN infeed, expect in Room D1', function() {
        it('inbound1-1 Single SSCC via RF Single Pallet', async function() { 
            this.timeout(400*1000);   
            await inbound.inbound1_1();
        });
        it('inbound1-2 4 SSCCs via RF Dual Pallet', async function() { 
            this.timeout(400*1000); 
            await inbound.inbound1_2()  
        });
    });

    describe('inbound2-1 4 SSCC into N10 of RECV01 via BCIN infeed. expect Room B or C', function() {
        it('inbound2-1 4 SSCCs via RF Single Pallet', async function() { 
            this.timeout(600*1000); 
            await inbound.inbound2_1()  
        });
    });

    describe('inbound2-2  HIFC ASN update test', function() {
        it('inbound2-2 4 SSCCs via RF Single Pallet', async function() { 
            this.timeout(600*1000); 
            await inbound.inbound2_2() 
        });
        it('inbound2-3 4 SSCCs via RF Single Pallet', async function() { 
            this.timeout(600*1000); 
            await inbound.inbound2_3() 
        });
    });


    afterEach(async function() {
        console.log('After Each')
    });

});


describe('Picking', function() {
   
    beforeEach(async function(){
        console.log('Before Each');
        clearAll();
        
    })

    describe('test', function(){
        it('test-temp', async function() { 
            this.timeout(3*60*1000);   
            await inbound.test_escape_key()
        });
    });

    describe('picking1', function() {
        it('picking1-1 Single SSCC order', async function() { 
            this.timeout(3*60*1000);   
            await inbound.inbound1_1();
            await picking.picking1_1();
        });

        

    });

    //room to room transfer orders
    describe('picking2', function(){
        it('picking2-1 Single SSCC order - Temperator Change - TRANSFER wave', async function() { 
            this.timeout(3*60*1000);   
            await inbound.inbound1_1();
            await picking.picking2_1();
        });
    });


    afterEach(async function() {
        console.log('After Each')
    });

});


describe('AsrsOptimisation', function() {
   
    beforeEach(async function(){
        console.log('Before Each');
        clearAll();
        
    })

    describe('skuchang1', function(){
        it('skuchange1-1 , change sku then Relocate them', async function() { 
            this.timeout(10*60*1000);//10min timeout of the test case   
            //Receive 4 pallets
            await inbound.inbound2_1();
            await skuchange.skuchange_1_1();
        });
        
        it('skuchange1-2 , change sku then Relocate them', async function() { 
            this.timeout(10*60*1000);//10min timeout of the test case   
            //Receive 4 pallets
            await inbound.inbound2_1();
            await skuchange.skuchange_1_1();
            await picking.picking1_4();
        });

    });

    afterEach(async function() {
        console.log('After Each')
    });

});