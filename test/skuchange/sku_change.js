
const {Builder, Key} = require('selenium-webdriver');
const {login,selectMenu, 
    sendHIFCMessage, 
    expectInventory} = require('../util.js');
const {asnList, ssccList, itemList} = require('../test_data.js')



function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

exports.skuchange_1_1 = async function(){
    const driver = await new Builder().forBrowser('chrome').build();
    await sendHIFCMessage('./test/skuchange/sku_change1.xml');
    await expectInventory(driver, ssccList.sscc1, itemList.item1, ['Room B', 'Room C'], 6000);
    await driver.quit();
}