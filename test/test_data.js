module.exports.HOST_IP = '192.168.86.121';
module.exports.HOST_DB = 'aWARDS_NG';
module.exports.DB_USERNAME = 'postgres';

module.exports.ssccList ={
    sscc1:'00194145170033500001',
    sscc2:'00194145170033500002',
    sscc3:'00194145170033500003',
    sscc4:'00194145170033500004',
    sscc5:'00194145170033500005',
    sscc6:'00194145170033500006',
    sscc7:'00194145170033500007',
    sscc8:'00194145170033500008',
    sscc9:'00194145170033500009',
    sscc10:'00194145170033500010'
}

module.exports.asnList = {
    asn1:'20210201',
    asn2:'20210202',
    asn3:'20210203',
    asn4:'20210204',
    asn5:'20210205',
    asn6:'20210206'
}

module.exports.itemList = {
    item1:'1013928800002031',
    item2:'1013928800002032',
    item3:'1013928800002033',
    item4:'1013928800002034',
    item5:'1013928800002035',
    item6:'1013928800002036',
}

module.exports.orderList = {
    order1:'order1',
    order2:'order2',
    order3:'order3',
    order4:'order4',
    order5:'order5',
    order6:'order6',
}