const {Builder} = require('selenium-webdriver');
const {login,selectMenu, 
    sendHIFCMessage, 
    expectOrderNumber, 
    releaseWave,
    expectElementContainsText, 
    userInput,
    clickF1,
    expectSSCCInAreaList} = require('../util.js');
const {orderList, ssccList} = require('../test_data.js')


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

exports.picking1_1 = async function(){

    const driver = await new Builder().forBrowser('chrome').build();
    await login(driver)
    await sendHIFCMessage('./test/picking/order1.xml');
    await expectOrderNumber(driver, orderList.order1)
    await releaseWave(orderList.order1)
    await clickF1(driver)
    await selectMenu(driver, 'Logout')
    await driver.quit();
}

exports.picking1_4 = async function(){

    const driver = await new Builder().forBrowser('chrome').build();
    await login(driver)
    await sendHIFCMessage('./test/picking/order1_4.xml');
    await expectOrderNumber(driver, orderList.order1)
    await releaseWave(orderList.order1)
    await clickF1(driver)
    await selectMenu(driver, 'Logout')
    await driver.quit();
}

//Temperatur Transfer , wave type TRANSFER
exports.picking2_1 = async function(){
    const driver = await new Builder().forBrowser('chrome').build();
    await login(driver)
    await sendHIFCMessage('./test/picking/order1_2_transfer.xml');
    await expectOrderNumber(driver, orderList.order1)
    await releaseWave(orderList.order1)
    await clickF1(driver)
    await selectMenu(driver, 'Logout')
    await driver.quit();
}