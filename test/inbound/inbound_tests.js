
const {Builder, Key} = require('selenium-webdriver');
const {login,selectMenu, 
    sendHIFCMessage, 
    expectAsnName, 
    expectElementContainsText, 
    userInput,
    userInputSpecialKey,
    clickF1,
    expectSSCCInAreaList,
    expectSQLResultCount} = require('../util.js');
const {asnList, ssccList} = require('../test_data.js');
const { utils } = require('mocha');



function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

exports.inbound1_1 = async function(){
    const driver = await new Builder().forBrowser('chrome').build();
    await sendHIFCMessage('./test/inbound/asn1.xml');
    await expectAsnName(driver, asnList.asn1)

    await login(driver)
    await selectMenu(driver, 'Stock Entry')
    await selectMenu(driver, 'Single Pallet')
    await expectElementContainsText(driver, 'content1', 'Scan SSCC')
    await userInput(driver, ssccList.sscc1)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')
    //logout
    await userInputSpecialKey(driver, Key.ESCAPE)
    await clickF1(driver)
    await selectMenu(driver, 'Logout')

    await expectSSCCInAreaList(driver, ssccList.sscc1, ['Room B'], 60*1000)

    await driver.quit();
}

module.exports.test_escape_key = async function(){
    const driver = await new Builder().forBrowser('chrome').build();
    //login process
    await login(driver)
    await selectMenu(driver, 'Stock Entry')
    await selectMenu(driver, 'Single Pallet')
    
    //logout process
    await userInputSpecialKey(driver, Key.ESCAPE)
    await clickF1(driver)
    await selectMenu(driver, 'Logout')

    driver.quit()
}

exports.inbound1_2 = async function(){
    const driver = await new Builder().forBrowser('chrome').build();
    await sendHIFCMessage('./test/inbound/asn1.xml');
    await expectAsnName(driver, asnList.asn1)

    await login(driver)
    await selectMenu(driver, 'Stock Entry')
    await selectMenu(driver, 'Dual Pallet')

    await expectElementContainsText(driver, 'content1', 'Scan SSCC 1')
    await userInput(driver, ssccList.sscc1)
    await expectElementContainsText(driver, 'content1', 'Scan SSCC 2')
    await userInput(driver, ssccList.sscc2)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')

    await expectElementContainsText(driver, 'content1', 'Scan SSCC 1')
    await userInput(driver, ssccList.sscc3)
    await expectElementContainsText(driver, 'content1', 'Scan SSCC 2')
    await userInput(driver, ssccList.sscc4)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')
    
    await userInputSpecialKey(driver, Key.ESCAPE)
    await clickF1(driver)
    await selectMenu(driver, 'Logout')

    await expectSSCCInAreaList(driver, ssccList.sscc1, ['Room B'], 60*1000)
    await expectSSCCInAreaList(driver, ssccList.sscc2, ['Room B'], 60*1000)
    await expectSSCCInAreaList(driver, ssccList.sscc3, ['Room B'], 60*1000)
    await expectSSCCInAreaList(driver, ssccList.sscc4, ['Room B'], 60*1000)

    

    await driver.quit();
}

exports.inbound2_1 = async function(){
    const driver = await new Builder().forBrowser('chrome').build();
    await sendHIFCMessage('./test/inbound/asn2.xml');
    await expectAsnName(driver, asnList.asn2)

    await login(driver)
    await selectMenu(driver, 'Stock Entry')
    await selectMenu(driver, 'Single Pallet')
    
    await expectElementContainsText(driver, 'content1', 'Scan SSCC')
    await userInput(driver, ssccList.sscc1)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')
    await sleep(2*1000)

    await expectElementContainsText(driver, 'content1', 'Scan SSCC')
    await userInput(driver, ssccList.sscc2)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')
    await sleep(2*1000)

    await expectElementContainsText(driver, 'content1', 'Scan SSCC')
    await userInput(driver, ssccList.sscc3)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')
    await sleep(2*1000)

    await expectElementContainsText(driver, 'content1', 'Scan SSCC')
    await userInput(driver, ssccList.sscc4)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')

    //logout
    await userInputSpecialKey(driver, Key.ESCAPE)
    await clickF1(driver)
    await selectMenu(driver, 'Logout')
    
    await expectSSCCInAreaList(driver, ssccList.sscc1, ['Room B'], 80*1000)
    await expectSSCCInAreaList(driver, ssccList.sscc2, ['Room B'], 80*1000)
    await expectSSCCInAreaList(driver, ssccList.sscc3, ['Room B'], 80*1000)
    await expectSSCCInAreaList(driver, ssccList.sscc4, ['Room B'], 80*1000)
    await driver.quit();
}


exports.inbound2_2 = async function(){
    const driver = await new Builder().forBrowser('chrome').build();
    await sendHIFCMessage('./test/inbound/asn2.xml');
    await expectAsnName(driver, asnList.asn2)

    await login(driver)
    await selectMenu(driver, 'Stock Entry')
    await selectMenu(driver, 'Single Pallet')
    
    await expectElementContainsText(driver, 'content1', 'Scan SSCC')
    await userInput(driver, ssccList.sscc1)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')
    await sleep(2*1000)

    await expectElementContainsText(driver, 'content1', 'Scan SSCC')
    await userInput(driver, ssccList.sscc2)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')
    await sleep(2*1000)

    await expectElementContainsText(driver, 'content1', 'Scan SSCC')
    await userInput(driver, ssccList.sscc3)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')
    await sleep(2*1000)

    await expectElementContainsText(driver, 'content1', 'Scan SSCC')
    await userInput(driver, ssccList.sscc4)
    await expectElementContainsText(driver, 'content1', 'BCIN')
    await userInput(driver, 'BCIN')

    //logout
    await userInputSpecialKey(driver, Key.ESCAPE)
    await clickF1(driver)
    await selectMenu(driver, 'Logout')
    
    await expectSSCCInAreaList(driver, ssccList.sscc1, ['Room B'], 80*1000)
    await expectSSCCInAreaList(driver, ssccList.sscc2, ['Room B'], 80*1000)
    await expectSSCCInAreaList(driver, ssccList.sscc3, ['Room B'], 80*1000)
    await expectSSCCInAreaList(driver, ssccList.sscc4, ['Room B'], 80*1000)

    //Sending an another ASN but full pallet qty is different. Expect an reject
    await sendHIFCMessage('./test/inbound/asn2_1.xml');

    await expectSQLResultCount(driver, `SELECT * FROM genhosterr_dtl gd WHERE gd.error_type = 'Error'`, 1)
    await driver.quit();
}

exports.inbound2_3 = async function(){
    const driver = await new Builder().forBrowser('chrome').build();
    await sendHIFCMessage('./test/inbound/asn2.xml');
    await expectAsnName(driver, asnList.asn2)

    //Sending an another ASN but full pallet qty is different. Expect an reject
    await sendHIFCMessage('./test/inbound/asn2_1.xml');

    await expectSQLResultCount(driver, `SELECT * FROM genhosterr_dtl gd WHERE gd.error_type = 'Error'`, 0)
    await expectSQLResultCount(driver, `SELECT * FROM item_groups ig JOIN item_group_uom igu ON ig.group_id  = igu.group_id 
        WHERE ig.group_name = '1013928800002031' AND igu.uom = 'BAG'`, 1)

    await expectSQLResultCount(driver, `SELECT * FROM item_groups ig JOIN item_group_uom igu ON ig.group_id  = igu.group_id 
    WHERE ig.group_name = '1013928800002031' AND igu.uom = 'PL' AND igu.uom_qty = 60`, 1)

    await driver.quit();
}
