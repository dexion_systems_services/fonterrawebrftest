Intro:
1. nodejs test framework: mocha
2. using selenium nodejs to execute browser based test case. 
3.1 Selenium uses chrome driver to load the browser. The chrome driver version has to match chrome version installed. 

How to execute the test cases:
1. To run all test cases:
npm test 
2. To run a specific or a group of test cases 
npm test -- --grep "testcasename" eg. npm test -- --grep "inbound1-1" 